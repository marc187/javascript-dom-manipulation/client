const token = localStorage.getItem(`token`)
const isAdmin = localStorage.getItem(`isAdmin`)
const courseContainer = document.querySelector(`#anyContainer`)
const adminButton = document.querySelector(`#adminButton`)

let courses;
let cardFooter;
console.log(typeof isAdmin)
if(isAdmin == `false`){
    fetch(`http://localhost:3000/api/courses/isActive`, {
        method: "GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
    })
    .then(result => result.json())
    .then(result => {
        console.log(result)

        if(result.length <= 0){
            return `No courses available`
        }else{
            courses = result.map(courses => {
                // console.log(courses)
                const {courseName, description, price} = courses
                return(
                    `
                        <div class="col-12 col-md-4">
                            <div class="card" style="width: 18 rem;">
                                <div class="card-body">
                                    <h5 class="card-title">${courseName}</h5>
                                    <p class="card-text text-left">${description}</p>
                                    <p class="card-text text-right">${price}</p>
                                    <a class="btn btn-info" href="./specificCourse.html?courseId=">Select Course</a>
                                </div>
                            </div>
                        </div>
                    `
                )
            }).join("")
            console.log(courses)
            courseContainer.innerHTML = courses
        }
           
        
    })
}else{
    console.log(`admin`)
    fetch(`http://localhost:3000/api/courses`, {
        method: "GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
    })
    .then(result => result.json())
    .then(result => {
        console.log(result)

        if(result.length <= 0){
            return `No courses available`
        }else{
            courses = result.map(courses => {
                // console.log(courses)
                const {courseName, description, price, isOffered, _id} = courses

                if(isOffered){
                    cardFooter = 
                    `
                        <a class="btn btn-info btn-block" href="./editCourse.html?courseId=${_id}">Edit</a>
                        <a class="btn btn-warning btn-block" href="./archive.html?courseId=${_id}">Archived</a>
                        <a class="btn btn-secondary btn-block" href="./delete.html?courseId=${_id}">Delete</a>
                    `
                }else{
                    cardFooter = 
                    `
                        <a class="btn btn-info btn-block" href="./archive.html?courseId=${_id}">Unarchived</a>
                        <a class="btn btn-info btn-block" href="./delete.html?courseId=${_id}">Delete</a>
                    `
                }

                return(
                    `
                        <div class="col-12 col-md-4">
                            <div class="card" style="width: 18 rem;">
                                <div class="card-body">
                                    <h5 class="card-title">${courseName}</h5>
                                    <p class="card-text text-left">${description}</p>
                                    <p class="card-text text-right">${price}</p>
                                    <div class="card-footer">${cardFooter}</div>
                                </div>
                            </div>
                        </div>
                    `
                )
            }).join("")
            console.log(courses)
            courseContainer.innerHTML = courses
            adminButton.innerHTML = 
            `<a class="btn btn-info m-3 ml-auto" href="./createCourse.html">Add Course</a>
            <a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>`
        }
    })
    
}
        
