let params = new URLSearchParams(document.location.search)
console.log(params)
const courseId = params.get(`courseId`)
console.log(courseId)

const token =localStorage.getItem(`token`)

const courseName = document.querySelector(`#couserName`)
const description = document.querySelector(`#description`)
const price = document.querySelector(`#price`)

fetch(`http://localhost:3000/api/courses/${courseId}`, {
    method: "GET",
    headers: {
        "Authorization": `Bearer ${token}`
    }
})
.then(result => result.json())
.then(result => {
    console.log(result)
    courseName.value = result.courseName
    description.value = result.description
    price.value = result.price
})

const editCourseForm = document.querySelector(`#editCourse`)

editCourseForm.addEventListener(`submit`, (e) => {
    e.preventDefault()

    fetch(`http://localhost:3000/api/courses/${courseId}/update`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
            courseName: courseName.value,
            description: description.value,
            price: price.value

        })
    })
    .then(result => result.json())
    .then(result => {
        console.log(result)
        if(result){
            alert(`Course successfully update.`)
            window.location.replace(`./course.html`)
        }else{
            alert(`Something went wrong. Please try again.`)
        }
        

    })
})