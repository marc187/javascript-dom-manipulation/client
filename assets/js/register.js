// target the elements
const registerForm = document.querySelector("#register");

//element.addEventListener(`event`, () => {})
registerForm.addEventListener("submit", (e) => {
    e.preventDefault()
    
    const firstName = document.querySelector("#firstName").value;
    const lastName = document.querySelector("#lastName").value;
    const email = document.querySelector("#email").value;
    const password = document.querySelector("#password").value;
    const cpw = document.querySelector("#cpw").value;
    
    //condition pw should match confirm pw
    if(password === cpw){
        
        //send the request to the server
        //fetch(URL, {options})
        fetch(`http://localhost:3000/api/users/email-exists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password
            })
        })
        .then(result => result.json())
        .then(result => {
            if(result == false){
                fetch(`http://localhost:3000/api/users/register`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName,
                        lastName,
                        email,
                        password
                    })
                })
                .then(result => result.json())
                .then(result => {
                    if(result){
                        alert(`User successfully registered`)
                        window.location.replace(`./login.html`)
                    }else{
                        alert(`Please try again.`)
                    }
                })
            } else{
                alert(`User already exists.`)
            }
        })
    }
})