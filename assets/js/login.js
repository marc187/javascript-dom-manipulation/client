const loginForm = document.querySelector(`#login`)

loginForm.addEventListener(`submit`, (e) => {
    e.preventDefault()

    const email = document.querySelector(`#email`).value
    const password = document.querySelector(`#password`).value
    const cpw = document.querySelector(`#cpw`).value

    if(password === cpw){
        fetch(`http://localhost:3000/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({email, password})
        })
        .then(result => result.json())
        .then(result => { 
            console.log(result)
            
            if(result){
                localStorage.setItem(`token`, result.token)
                let token = localStorage.getItem(`token`)
                fetch(`http://localhost:3000/api/users/profile`, {
                    method:"GET",
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                })
                .then(result => result.json())
                .then(result => { //result id object with user's data
                    console.log(result) //result is token with bearer
                    localStorage.setItem(`isAdmin`, result.isAdmin)
                    localStorage.setItem(`id`, result._id)
                    alert(`Login successfully`)
                    window.location.replace(`../pages/courses.html`)
                })
            }else{
                alert(`Something went wrong. Please try again.`)
            }
        })
    }else{
        alert(`Please confirm password.`)
    }
})