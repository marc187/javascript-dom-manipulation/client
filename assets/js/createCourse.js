const createCourseForm = document.querySelector(`#createCourse`)

const token = localStorage.getItem(`token`)


createCourseForm.addEventListener(`submit`, (e) => {
    e.preventDefault()
    const courseName = document.querySelector(`#courseName`).value
    const description = document.querySelector(`#description`).value
    const price = document.querySelector(`#price`).value
    console.log(token)
    fetch(`http://localhost:3000/api/courses/create`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
            courseName, description, price
        })    
    })
    .then(result => result.json())
    .then(result => {
        console.log(result)

        if(result){
            alert(`Course successfully added`)
            window.location.replace(`./courses.html`)
        }else{
            
        }
    })
})

